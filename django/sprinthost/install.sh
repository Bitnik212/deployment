#!/bin/bash

base_url=$1

# Download .htaccess
wget $base_url/.htaccess

# Setup venv
rm -rf venv
python -m venv venv

# Clean public_html
rm -rf ../public_html/*

# Add uwsgi staff
mv deploy/dev/activate_this.py venv/bin/
mv deploy/dev/index.wsgi ../public_html/

# App setup
set -e 
source venv/bin/activate

pip install -r requirements.txt
python manage.py makemigrations
python manage.py migrate

deactivate
